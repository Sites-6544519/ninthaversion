function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

function addNewNote(buttonLoc, noteLoc) {
  var date = new Date;
  var RanRan = Math.floor(Math.random() * 999999);

  var NewButtonName = "Note-" + RanRan;
  var NewFunc = "openCity(event, 'Note-" + RanRan + "')";

  // Button Creation
  var disArea = buttonLoc;
  var buttonArea = document.getElementById(disArea);
  var element = document.createElement("button");
  element.setAttribute("class", "tablinks");
  element.textContent = NewButtonName;
  element.setAttribute("onclick", NewFunc);

  buttonArea.appendChild(element);


  // Note Creation
  var noteArea = document.getElementById(noteLoc);
  var newDiv = document.createElement("div");
  newDiv.setAttribute("id", NewButtonName);
  newDiv.setAttribute("class", "tabcontent");

  var newSpan = document.createElement("span");
  newSpan.setAttribute("onclick", "this.parentElement.style.display='none'");
  newSpan.setAttribute("class", "topright");
  newSpan.textContent = '×';

  var startHour = date.getHours();
  var startMin  = date.getMinutes();

  if (startMin < 10)
    startMin = 0+""+startMin;

  var newTitle = document.createElement("h3");
  newTitle.setAttribute("id", NewButtonName + "Title");
  var nnn = NewButtonName + "  (" + startHour + ":" + startMin + ")";
  newTitle.textContent = nnn;

  var newTextArea = document.createElement("textarea");
  newTextArea.setAttribute("name", NewButtonName);
  newTextArea.setAttribute("rows", "15");
  newTextArea.setAttribute("cols", "73");
  newTextArea.setAttribute("placeholder", "Enter Notes Here...");


  // Stop Button

  var newStopDiv = document.createElement("div");
  newStopDiv.setAttribute("class", "tab");

  var stopFuncID = "stopFunc('Note-" + RanRan + "Title')";
  var newStopButton = document.createElement("button");
  newStopButton.setAttribute("class", "tablinks");
  newStopButton.textContent = "Stop";
  newStopButton.setAttribute("onclick", stopFuncID);



  newDiv.appendChild(newSpan);
  newDiv.appendChild(newTitle);
  newDiv.appendChild(newTextArea);
  newStopDiv.appendChild(newStopButton);
  newDiv.appendChild(newStopDiv);
  noteArea.appendChild(newDiv);

  element.click();
}


function stopFunc(titleLoc) {
  var date = new Date;
  var endHour = date.getHours();
  var endMin  = date.getMinutes();

  if (endMin < 10)
    endMin = 0+""+endMin;


  var origText = document.getElementById(titleLoc);
  let preText = origText.innerHTML;
  let text = preText.trim();
  var tmpTextArr = text.split(" ");
  var newStopTime = " -> (" + endHour + ":" + endMin + ")";

  if (tmpTextArr.length == 3)
    origText.textContent = tmpTextArr[0] + " " + tmpTextArr[2] + " " + newStopTime;
  else
    origText.textContent = tmpTextArr[0] + " " + tmpTextArr[1] + " " + newStopTime;

//  var newStopTime = document.createTextNode(" -> (" + endHour + ":" + endMin + ")");
//  origText.appendChild(newStopTime);
}




function ckLoad() {
  
}

function ckSave() {
  
}

function ckImport() {
  
}

function ckExport() {
  
}



// Get the element with id="defaultOpen" and click on it
// document.getElementById("defaultOpen").click();
