+++
author = "Hehehe"
title = "Notes Tool"
date = "2023-05-11"
description = "A notes tool made on Hugo"
tags = [
    "tools",
]
+++

<script src="/js/notes-tool/scripts.js"></script>
<link rel="stylesheet" href="/css/notes-tool/styles.css">


Quick Buttons

<div id="importArea">
  <button class="tablinks" onclick="console.log('Load Notes')">Load Notes</button>
  <button class="tablinks" onclick="console.log('Write Notes')">Write Notes</button>
  <button class="tablinks" onclick="console.log('Import Notes')">Import Notes</button>
</div>

***



## Notes
### Click on the buttons inside the tabbed menu:

<div class="tab" id="buttonsArea">
  <!---
  <button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen">London</button>
  --->
  <button class="tablinks" onclick="openCity(event, 'Note-04')">Ex</button>
  <div class="tablinks tabAdd">
    <button class="tabAdd" onclick="addNewNote('buttonsArea', 'StuffHere')">+</button>
  </div>
</div>


<div id="StuffHere">
  <!---
  <div id="London" class="tabcontent">
    <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
    <h3>London</h3>
    <p>London is the capital city of England.</p>
  </div>
  --->

  <div id="Note-04" class="tabcontent">
    <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
    <h3>Example Note</h3>
​    <textarea id="w3review" name="w3review" rows="15" cols="73" placeholder="Enter Notes Here..."></textarea>
    <div class="tab" id="buttonsAreas">
      <button class="tablinks center">Stop</button>
    </div>
  </div>
</div>



